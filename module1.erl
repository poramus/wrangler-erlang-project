-module(module1).

-export([process/1,processes/1]).

process(N) ->
	module2:do(N).
processes(N) ->
	module2:do(N),
	module3:do(N-1).