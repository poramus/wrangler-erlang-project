
-module(pingpong).

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0, stop/0, play/1]).
-export([init_a/0,init_b/0]).
-export([recursion/0]).	

start() ->
	register(ping,spawn(pingpong,init_a,[])),
	register(pong,spawn(pingpong,init_b,[])),
	ok.

stop() ->
	ping ! stop,
	pong ! stop,
	ok.

play(N) ->
	ping ! N,
	ok.

recursion() ->
    receive
	N when N>0 ->
	    recursion(),
	    io:write(N);
	_  -> ok
    end.


%% ====================================================================
%% Internal functions
%% ====================================================================

init_a() ->
	ping().

init_b() ->
	pong().

ping() ->
	receive
		stop -> io:format("Ping stopped~n"),
				ok;
		0 -> io:format("Ping 0~n"),
			 ping();
		N -> io:format("Ping ~B~n", [N]),
			 timer:sleep(500),
			 pong ! N-1,
			 ping()
	after
		20000 -> io:format("Ping bored~n"),
				 ok
	end.

pong() ->
	receive
		stop -> io:format("Pong stopped~n"),
				ok;
		0 -> io:format("Pong 0~n"),
			 pong();
		N -> io:format("Pong ~B~n", [N]),
			 timer:sleep(500),
			 ping ! N-1,
			 pong()
	after
		20000 -> io:format("Pong bored~n"),
				 ok
	end.
